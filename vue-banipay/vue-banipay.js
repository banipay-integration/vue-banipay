"use strict";
import axios from "axios";
var _createClass = (function () {
    function t(t, e) {
        for (var i = 0; i < e.length; i++) {
            var n = e[i];
            (n.enumerable = n.enumerable || !1), (n.configurable = !0), "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n);
        }
    }
    return function (e, i, n) {
        return i && t(e.prototype, i), n && t(e, n), e;
    };
})();
function _classCallCheck(t, e) {
    if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
}
var forEach = function (t, e) {
        return Array.isArray(t)
            ? t.forEach(e)
            : Object.keys(t).map(function (i) {
                  return e(t[i], i);
              });
    },
    clone = function (t) {
        return t ? JSON.parse(JSON.stringify(t)) : t;
    },
    BanipayCheckout = (function () {
        function t(e) {
            _classCallCheck(this, t), (this._widgetId = "banipay-widget-" + Date.now()), (this._config = e), this.createPaymentLink();
        }
        return (
            _createClass(t, [
                {
                    key: "handle_msg_widgetInit",
                    value: function () {
                        this.widgetInit();
                    },
                },
                {
                    key: "widgetInit",
                    value: function () {
                        var t = clone(this._config);
                        delete t.eventHandler, this.msgWidget("paymentInfo", t);
                    },
                },
                {
                    key: "handle_msg_walletPaymentVerification",
                    value: function (t) {
                        this._config.eventHandler.onSuccess(t), this.hide();
                    },
                },
                {
                    key: "handle_msg_widgetError",
                    value: function (t) {
                        var e = this._config.eventHandler.onError;
                        e && e(t);
                    },
                },
                {
                    key: "disableParentScrollbar",
                    value: function () {
                        (this.parentOverflowValue = window.document.body.style.overflowY), (window.document.body.style.overflowY = "hidden");
                    },
                },
                {
                    key: "enableParentScrollbar",
                    value: function () {
                        (window.document.body.style.overflowY = this.parentOverflowValue), (this.parentOverflowValue = null);
                    },
                },
                {
                    key: "show",
                    value: function (t) {
                        (this._config.source = "web"), this._widget.setAttribute("src", this._paymentUrl), Object.assign(this._config, t), this.disableParentScrollbar(), (this._widget.style.display = "block"), this.widgetInit();
                    },
                },
                {
                    key: "handle_msg_hide",
                    value: function () {
                        this.hide();
                        var t = this._config.eventHandler.onClose;
                        t && t();
                    },
                },
                {
                    key: "hide",
                    value: function () {
                        this.enableParentScrollbar(), (this._widget.style.display = "none");
                    },
                },
                {
                    key: "createPaymentLink",
                    value: function () {
                        var t = "v2";
                        "Dev" == this._config.environmentType && (t = "staging");
                        var e = new Date(),
                            i = new Date(e.getTime() + 18e5),
                            n = this._config.bdd;
                        n.push({ title: "successUrl", description: this._config.successUrl, data: this._config.successUrl, status: "true", typeDate: e.toISOString() }),
                            n.push({ title: "failUrl", description: this._config.failUrl, data: this._config.failUrl, status: "true", typeDate: e.toISOString() }),
                            n.push({ title: "notificationUrl", description: this._config.notificationUrl, data: this._config.notificationUrl, status: "true", typeDate: e.toISOString() });
                        const a = {
                            affiliate: this._config.affiliateKey,
                            amount: this._config.amount,
                            business: this._config.businessKey,
                            code: this._config.externalCode,
                            currency: this._config.currency,
                            description: this._config.description,
                            expirationDate: i.toISOString(),
                            productName: this._config.description,
                            bdd: JSON.stringify({ data: n }),
                        };
                        axios
                            .post(`https://${t}.banipay.me/api/pagos/link-payment`, a, { headers: { Accept: "application/json", "Content-Type": "application/json" } })
                            .then((e) => {
                                var i = e.data;
                                (this._paymentUrl = `https://${t}.banipay.me/modal/${i.id}?host=${location.origin}`), (this._widget = this.attachWidget());
                            })
                            .catch((t) => {
                                console.log(t);
                            });
                    },
                },
                {
                    key: "attachWidget",
                    value: function () {
                        var t = window.document.createElement("iframe");
                        return (
                            t.setAttribute("id", this._widgetId),
                            (t.style.position = "fixed"),
                            (t.style.display = "block"),
                            (t.style.top = "10%"),
                            (t.style.left = "15%"),
                            (t.width = "70%"),
                            (t.height = "80%"),
                            t.setAttribute("src", this._paymentUrl),
                            (t.style.zIndex = 999999999),
                            t.setAttribute("frameborder", 0),
                            t.setAttribute("allowtransparency", !0),
                            window.document.body.appendChild(t),
                            t
                        );
                    },
                },
            ]),
            t
        );
    })();
export default BanipayCheckout;
