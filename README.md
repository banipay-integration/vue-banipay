# Vue BaniPay

#### Introducción
VueJS component wrapper for [BaniPay](https://banipay.me)

El único requisito es axios.

##### En el template

```
<template>
	<div>
		       <vue-banipay v-bind="banipayConfig" labelButton="Pay Now!" classButton="btn btn-primary"/>
	</div>
</template>
```
#### Establecer las variables

```javascript
import VueBanipay from '@/plugins/vue-banipay/Banipay.vue'

export default {
  components: {
    VueBanipay
  },
  data() {
      return {
        banipayConfig: {
            "amount": 10,
            "currency": "BOB",
            "environmentType": "Dev",
            "businessKey": "02e4b31f-20bd-43f9-9f2d-3ef7733f2d0f",
            "affiliateKey": "0ef291bf-0d70-444a-b949-b4144bba6557",
            "successUrl": "https://banipay.me",
            "failUrl": "https://banipay.me",
            "notificationUrl": "https://banipay.me",
            "description": "Mi producto",
            "externalCode": "ABC000"
        }
      }
  },
}
```

